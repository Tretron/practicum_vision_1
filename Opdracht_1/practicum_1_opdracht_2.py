# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 13:23:05 2018

@author: tretron
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 13:18:53 2018

@author: tretron
"""

import cv2
import numpy as np
import scipy


frame = cv2.imread("3_3.jpg")
frame = scipy.misc.imresize(frame, 0.5)
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

print("please select the colour. 0 = Red, 1 = pink, 2 = purple, 3= Blue, 4= Yellow, 5= green")
colour_selected = int(input("colour = "))
if colour_selected == 0:
    lower = np.array([0,140,140])
    upper = np.array([19,255,255])
elif colour_selected == 1:
    lower = np.array([165,120,120])
    upper = np.array([170,255,255])
elif colour_selected == 2:
    lower = np.array([130,50,50])
    upper = np.array([160,255,255])
elif colour_selected == 3:
    lower = np.array([80,50,50])
    upper = np.array([130,255,255])
elif colour_selected == 4:
    lower = np.array([20,100,100])
    upper = np.array([35,255,255])
elif colour_selected == 5:
    lower = np.array([40,100,100])
    upper = np.array([70,255,255])
else:
    print("colour does not exist.")

mask = cv2.inRange(hsv, lower, upper)
res = cv2.bitwise_and(frame,frame, mask= mask)

kernel = np.ones((5,5),np.uint8)

opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
res = cv2.bitwise_and(frame,frame, mask= opening)
im2, contours, hierarchy = cv2.findContours(opening,1,2)

cv2.imshow('Original',frame)
cv2.imshow('Mask',mask)
cv2.imshow('re', res)
print('amount of objects with selected colour = ',len(contours))

cv2.waitKey(0)
cv2.destroyAllWindows()