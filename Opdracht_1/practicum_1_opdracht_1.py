# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 13:19:40 2018

@author: tretron
"""
import cv2
import time
import numpy as np
import math
import scipy

#cirkletransformatie


target_image = input("please select image to find (image must be png)  ")
image_grabbed = cv2.imread("%s.png" %target_image)
image_grabbed = scipy.misc.imresize(image_grabbed, 0.1)

img_grey = cv2.cvtColor(image_grabbed ,cv2.COLOR_BGR2GRAY)
cv2.imshow('frame',img_grey)
cv2.imshow("target image", image_grabbed)
cv2.waitKey(0)
cv2.destroyAllWindows()