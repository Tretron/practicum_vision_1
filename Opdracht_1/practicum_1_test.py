# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 13:18:53 2018

@author: tretron
"""

import cv2
import numpy as np
import scipy


frame = cv2.imread("1_2.jpg")
frame = scipy.misc.imresize(frame, 0.5)
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

print("please select the colour. 0 = Red, 1 = pink, 2 = purple, 3= Blue, 4= Yellow, 5= green")
colour_selected = int(input("colour = "))
if colour_selected == 0:
    lower = np.array([0,150,100])
    upper = np.array([10,255,180])
elif colour_selected == 1:
    lower = np.array([170,60,90])
    upper = np.array([180,255,255])
elif colour_selected == 2:
    lower = np.array([130,30,30])
    upper = np.array([160,255,255])
elif colour_selected == 3:
    lower = np.array([80,50,50])
    upper = np.array([130,255,255])
elif colour_selected == 4:
    lower = np.array([20,100,100])
    upper = np.array([32,255,255])
elif colour_selected == 5:
    lower = np.array([40,50,50])
    upper = np.array([60,255,255])
else:
    print("colour does not exist.")

mask = cv2.inRange(hsv, lower, upper)
res = cv2.bitwise_and(frame,frame, mask= mask)

kernel = np.ones((5,5),np.uint8)

opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
res = cv2.bitwise_and(frame,frame, mask= closing)
contours = cv2.findContours(closing,1,2)

cv2.imshow('Original',frame)
cv2.imshow('Mask',mask)
cv2.imshow('re', res)
print('amount of objects with selected colour = ',len(contours))

cv2.waitKey(0)
cv2.destroyAllWindows()