# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 13:18:53 2018

@author: tretron
"""

import cv2
import numpy as np
import scipy
import scipy.misc


frame = cv2.imread("3_6.jpg")
frame = scipy.misc.imresize(frame, 0.3)
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
cv2.imshow('Original',frame)

print("please select the colour. 0 = Red, 1 = pink, 2 = purple, 3= Blue, 4= Yellow, 5= green")
colour_selected = int(input("colour = "))
if colour_selected == 0:
    lower = np.array([0,70,70])
    upper = np.array([20,255,255])
elif colour_selected == 1:
    lower = np.array([160,70,70])
    upper = np.array([180,255,255])
elif colour_selected == 2:
    lower = np.array([130,70,70])
    upper = np.array([165,255,255])
elif colour_selected == 3:
    lower = np.array([80,50,50])
    upper = np.array([130,255,255])
elif colour_selected == 4:
    lower = np.array([20,100,100])
    upper = np.array([35,255,255])
elif colour_selected == 5:
    lower = np.array([40,100,100])
    upper = np.array([70,255,255])
else:
    print("colour does not exist.")
    lower = np.array([0,0,0])
    upper = np.array([255,255,255])

mask = cv2.inRange(hsv, lower, upper)
res = cv2.bitwise_and(frame,frame, mask= mask)

kernel = np.ones((10,10),np.uint8)

opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

contours, hierarchy = cv2.findContours(opening,1,2)
res = cv2.bitwise_and(frame,frame, mask= opening)
n=0
area = []
while n<=len(contours)-1:
    area.append(cv2.contourArea(contours[n]))
    n=n+1
n=0
count_contours = 0
while n<=len(area)-1:
    if area[n]>1100 and area[n]<4000:
        count_contours = count_contours+1
    n=n+1


cv2.imshow('opening', mask)
cv2.imshow('re', res)
print('amount of objects with selected colour = ',count_contours)

cv2.waitKey(0)
cv2.destroyAllWindows()