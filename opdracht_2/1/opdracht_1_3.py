# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 17:50:15 2018

@author: tretron
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
from tesseract import image_to_string




frame = cv2.imread("NL08KTD6.jpg")
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)


"""
Range of colour yellow.
"""
lower = np.array([10,100,100])
upper = np.array([70,255,255])

"""
remove all colours but yellow and make a bit mask from it.
"""
mask = cv2.inRange(hsv, lower, upper)

"""
remove noise and features not needed
"""

kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
inverted = cv2.bitwise_not(opening)




    
print(image_to_string(opening))
 
# show the output images
cv2.imshow("Image", frame)

cv2.waitKey(0)
cv2.destroyAllWindows()