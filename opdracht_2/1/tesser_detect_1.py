# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 17:31:21 2018

@author: tretron
"""

from PIL import Image
import pytesser as pt
import cv2
import numpy as np
 

frame = cv2.imread("NL08KTD6.jpg")
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

"""
Range of colour yellow.
"""
lower = np.array([10,100,100])
upper = np.array([70,255,255])

"""
remove all colours but yellow and make a bit mask from it.
"""
mask = cv2.inRange(hsv, lower, upper)

"""
remove noise and features not needed
"""

kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
inverted = cv2.bitwise_not(opening)

image_file = opening
im = Image.open(image_file)
text = pt.image_to_string(im)
text = pt.image_file_to_string(image_file)
text = pt.image_file_to_string(image_file, graceful_errors=True)
print("=====output=======")