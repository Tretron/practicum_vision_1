# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 16:52:58 2018

@author: tretron
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
import scipy


template = cv2.imread('template_D.PNG',0)
w, h = template.shape[::-1]
frame = cv2.imread("NL08KTD6.jpg")
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

"""
Range of colour yellow.
"""
lower = np.array([10,100,100])
upper = np.array([70,255,255])

"""
remove all colours but yellow and make a bit mask from it.
"""
mask = cv2.inRange(hsv, lower, upper)

"""
remove noise and features not needed
"""
kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

inverted = cv2.bitwise_not(opening)
"""
Start detection with template matching
"""


cv2.imshow("template", res)
cv2.imshow("filter", inverted)
cv2.imshow("origonal", frame)
cv2.waitKey(0)
cv2.destroyAllWindows()