import cv2 as cv
import numpy as np
import os
from PIL import ImageFont, ImageDraw, Image
def order_points(pts):
    rect = np.zeros((4, 2), dtype="float32")
    s = pts.sum(axis=2)
    #print (np.argmax(s))
    #print(pts[78],pts[2])
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    diff = np.diff(pts, axis=2)
    diff2 = np.diff(pts, axis=2)
    tempdiff = []
    tempdiff2 = []
    for i in range (0,(len(diff))):
        if diff[i][0][0] >= 2147483647:
            tempdiff2.append(4294967281 - diff2[i][0][0])
            #diff2[i][0][0] = 4294967281 - diff2[i][0][0]
            tempdiff.append(1)
            #diff[i][0][0] = 1
        elif diff[i][0][0] < 2147483647:
            tempdiff2.append(1)
            tempdiff.append(diff[i][0][0])
            #diff2[i][0][0] = 1

    #print(diff)
    print(tempdiff2)
    if np.amax(tempdiff) ==1:
        rect[3] = pts[np.argmin(tempdiff2)]
    else:
        rect[3] = pts[np.argmax(tempdiff)]
    rect[1] = pts[np.argmax(tempdiff2)]
    print(np.argmax(tempdiff2))

    print(rect)
    return rect


def four_point_transform(image, pts):
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")
    M = cv.getPerspectiveTransform(rect, dst)
    warped = cv.warpPerspective(image, M, (maxWidth, maxHeight))
    warped = cv.resize(warped,(1800,350))
    return warped


font = ImageFont.truetype("Kenteken.ttf", 275)
namestrings = "ABCDEFGHIJKLMNOPQRSTUVWQYZ1234567890-"
image = []
imageletter = []
tW = []
tH = []
for letter in range(0, len(namestrings)):
    image.append(Image.new("RGB", (350, 550)))
    draw = ImageDraw.Draw(image[letter])
    draw.text((0, 0), namestrings[letter], font=font)
    name = namestrings[letter] + ".png"
    image[letter].save(name)
    imageletter.append(cv.imread(name,0))
    imageletter[letter] = cv.Canny(imageletter[letter], 50, 200)
    tWtemp, tHtemp =imageletter[letter].shape[:2]
    tW.append(tW)
    tH.append(tH)
    imageletter_temp, contours_temp, hierarchy_temp = cv.findContours(imageletter[letter],cv.RETR_EXTERNAL,cv.CHAIN_APPROX_SIMPLE)
    try: hierarchy = hierarchy[0]
    except: hierarchy = []
    height, width = imageletter_temp.shape
    min_x, min_y = width, height
    max_x = max_y = 0
    for contour_temp, hierarchy_temp in zip(contours_temp, hierarchy_temp):
        (x, y, w, h) = cv.boundingRect(contour_temp)
        min_x, max_x = min(x, min_x), max(x + w, max_x)
        min_y, max_y = min(y, min_y), max(y + h, max_y)
        if w > 30 and h > 80 and w < 500 and h < 300:
            cv.rectangle(imageletter_temp, (x, y), (x + w, y + h), (255, 0, 0), 2)
            imageletter[letter] = imageletter_temp[y:y + h, x:x + w]
            #cv.imshow(name, imageletter[letter])


kernel = np.ones((5, 5), np.uint8)
kernel2 = np.ones((25,25),np.uint8)
output = np.zeros((1000,300),np.uint8)
path = "/home/timo/PycharmProjects/Vision/"
foto = cv.imread(path+"Serie2/IMG_1023_2.JPG")
hsv = cv.cvtColor(foto, cv.COLOR_RGB2HSV)
cv.imshow("image", hsv)
mask = cv.inRange(hsv, (85, 160, 160), (105, 255, 255))
mask = cv.erode(mask, kernel, iterations = 1)
mask = cv.dilate(mask, kernel2, iterations = 2)
mask = cv.erode(mask, kernel2, iterations = 1)
end_result = cv.bitwise_and(foto, foto, mask=mask)
gray = cv.cvtColor(end_result,cv.COLOR_BGR2GRAY)
gray = np.float32(gray)
dst = cv.cornerHarris(gray,5,13,0.04)
dst = cv.dilate(dst,None)
foto[dst > 0.01*dst.max()] = [0,0,255]
filename = "image2.png"
img2, contours, hierarchy = cv.findContours(mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
contours = contours[0]
#print(contours)
contours = np.array(contours,np.uint32)
warped = four_point_transform(end_result,contours)
hsv = cv.cvtColor(warped, cv.COLOR_RGB2HSV)
mask = cv.inRange(hsv, (85, 140, 140), (110, 255, 255))
cv.imshow("mask",hsv)
mask = cv.bitwise_not(mask)
mask = cv.erode(mask, kernel, iterations=3)
masker = cv.dilate(mask, kernel, iterations = 8)
canny_img = cv.Canny(masker,50,200)
im2, contours, hierarchy = cv.findContours(canny_img,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_SIMPLE)
#cv.imshow("img",img2)
#cv.imshow("img2",masker)
print(len(contours))
try: hierarchy = hierarchy[0]
except: hierarchy = []

height, width = canny_img.shape
min_x=  min_y = 0
max_x , max_y = width,height
imagelist = []
for contour, hierarchy in zip(contours, hierarchy):
    (x,y,w,h) = cv.boundingRect(contour)
    minx, maxx = max(x-25, min_x), min(x+w+25, max_x)
    miny, maxy = max(y-25, min_y), min(y+h+25, max_y)
    #print(minx,miny,maxx,maxy)
    #print(min_x,min_y,max_x,max_y)
    #print(width,height)
    if w > 30 and h > 80 and w < 500 and h < 300:
        #cv.rectangle(warped, (x,y), (x+w,y+h), (255, 0, 0), 2)
        makegray = cv.cvtColor(warped[miny:maxy, minx:maxx], cv.COLOR_BGR2GRAY)
        ht, makethres = cv.threshold(makegray,115,255,cv.THRESH_BINARY)
        lettertje = cv.bitwise_not(makethres)
        #cv.imshow("lala",makethres)
        kernel = np.ones((4, 4), np.float32) / 16
        lettertje = cv.filter2D(lettertje, -1, kernel)
        lettertje = cv.Canny(lettertje, 50, 200)
        imagelist.append(lettertje)
kenteken = ""
for j in range(0,len(imagelist)-1):
    found = None

    i=0
    for i in range(0, len(namestrings)):
        try:
            res = cv.matchTemplate(imagelist[j], imageletter[i], cv.TM_CCOEFF)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
            if found is None or max_val > found[0]:
                found = (max_val,max_loc,i)
                cv.imshow("naampje", imagelist[j])
                cv.imshow("thingy", imageletter[found[2]])
                k = cv.waitKey(0)
                if k == 27:  # wait for ESC key to exit
                    cv.destroyAllWindows()
        except:
            k=0
    #kenteken = kenteken+ namestrings[found[2]]
    print(namestrings[found[2]])

#cv.imshow("lost",imagelist[0])
#cv.imshow("warped",warped)
print(kenteken)
k = cv.waitKey(0)
if k == 27:  # wait for ESC key to exit
    cv.destroyAllWindows()
elif k == ord('s'):  # wait for 's' key to save and exit
    cv.imwrite('Filtered.png', image)
    cv.destroyAllWindows()