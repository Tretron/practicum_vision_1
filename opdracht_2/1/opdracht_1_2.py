# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 17:50:15 2018

@author: tretron
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

frame = cv2.imread("NL08KTD6.jpg")
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
template_k = cv2.imread("template_k.png")
template_k = cv2.cvtColor(template_k, cv2.COLOR_BGR2GRAY)
template_k = cv2.inRange(template_k, 0, 1)

template_8 = cv2.imread("template_8.png")
template_8 = cv2.cvtColor(template_8, cv2.COLOR_BGR2GRAY)
template_8 = cv2.inRange(template_8, 0, 1)

"""
Range of colour yellow.
"""
lower = np.array([10,100,100])
upper = np.array([70,255,255])

"""
remove all colours but yellow and make a bit mask from it.
"""
mask = cv2.inRange(hsv, lower, upper)

"""
remove noise and features not needed
"""

kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)


edged = cv2.Canny(opening, 10, 250)
"""
testing for contours in the image, using the opening image to filter out destortions
"""
im2, cnts, hierarchy = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
idx = 0
for c in cnts:
    x,y,w,h = cv2.boundingRect(c) 
    if w>50 and h>50:
        idx+=1
        new_img=opening[y:y+h,x:x+w]
        cv2.imwrite(str(idx) + '.png', new_img)

inverted = cv2.bitwise_not(new_img)

first = cv2.morphologyEx(inverted, cv2.MORPH_OPEN, template_k)
im2, contours, hierarchy = cv2.findContours(first,1,2)
image = inverted
if contours != 0:
    print(contours[0][0])
    
second = cv2.morphologyEx(inverted, cv2.MORPH_OPEN, template_8)
im3, contours, hierarchy = cv2.findContours(second,1,2)
image = inverted
if contours != 0:
    print(contours)
cv2.imshow("mew 2" ,second)
 
# show the output images
cv2.imshow("Image", frame)
cv2.imshow("Output", image)
cv2.imshow("template_k", im2)

cv2.waitKey(0)
cv2.destroyAllWindows()