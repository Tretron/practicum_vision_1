# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 17:50:15 2018

@author: tretron
"""

import cv2 
import numpy as np
import scipy

from header_file import transform

image = cv2.imread("IMG_0984.JPG")
image = scipy.misc.imresize(image, 0.5)
hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

"""
Range of colour yellow.
"""
lower = np.array([10,100,100])
upper = np.array([70,255,255])

"""
remove all colours but yellow and make a bit mask from it.
"""
mask = cv2.inRange(hsv, lower, upper)
kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)


cv2.imshow("mew", opening)


edged = cv2.Canny(opening, 10, 250)
"""
testing for contours in the image, using the opening image to filter out destortions
"""
im2, cnts, hierarchy = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
idx = 0
for c in cnts:
    x,y,w,h = cv2.boundingRect(c) 
    if w>50 and h>50:
        idx+=1
        new_img=opening[y:y+h,x:x+w]
        cv2.imwrite(str(idx) + '.png', new_img)
        cv2.imshow("im",image)

small_img=cv2.imread('1.png')

edge=cv2.Canny(small_img,100,200)
_,contours,_=cv2.findContours(edge.copy(),1,1)
cv2.drawContours(small_img,contours,-1,[0,255,0],2)
cv2.imshow('Contours',small_img)



# grab the (x, y) coordinates of all pixel values that
# are greater than zero, then use these coordinates to
# compute a rotated bounding box that contains all
# coordinates
coords = np.column_stack(np.where(small_img > 0))
angle = cv2.minAreaRect(coords)[-1]
 
# the `cv2.minAreaRect` function returns values in the
# range [-90, 0); as the rectangle rotates clockwise the
# returned angle trends to 0 -- in this special case we
# need to add 90 degrees to the angle
if angle < -45:
	angle = -(90 + angle)
 
# otherwise, just take the inverse of the angle to make
# it positive
else:
	angle = -angle

# rotate the image to deskew it
(h, w) = small_img.shape[:2]
center = (w // 2, h // 2)
M = cv2.getRotationMatrix2D(center, angle, 1.0)
rotated = cv2.warpAffine(small_img, M, (w, h),
	flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
cv2.imshow("rotated", rotated)



cv2.waitKey(0)
cv2.destroyAllWindows()