# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 16:40:15 2018

@author: tretron
"""

# import the necessary packages
from imutils import contours
import numpy as np
import argparse
import imutils
import cv2


# load the reference OCR-A image from disk, convert it to grayscale,
# and threshold it, such that the digits appear as *white* on a
# *black* background
# and invert it, such that the digits appear as *white* on a *black*
ref = cv2.imread("OCR_B.PNG")
ref = cv2.cvtColor(ref, cv2.COLOR_BGR2GRAY)
ref = cv2.threshold(ref, 10, 255, cv2.THRESH_BINARY_INV)[1]


#kenteken
frame = cv2.imread("NL08KTD6.jpg")
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
#Range of colour yellow.
lower = np.array([10,100,100])
upper = np.array([70,255,255])

"""
remove all colours but yellow and make a bit mask from it.
"""
mask = cv2.inRange(hsv, lower, upper)

"""
remove noise and features not needed
"""
kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)


cv2.imshow("reference", ref)
cv2.imshow("target", opening)
cv2.waitKey(0)
cv2.destroyAllWindows()