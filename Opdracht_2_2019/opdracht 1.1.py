# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 12:49:49 2019

@author: Laurens Roos

opdracht 1.1

Doel: Haal uit de eerste serie foto’s de nummerborden. Toon in het eerste window de orginele foto en in het
tweede window alleen het nummerbord.
"""
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 17:50:15 2018

@author: tretron
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
import pytesseract

# Uncomment the line below to provide path to tesseract manually
pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe"
 
# Define config parameters.
# '-l eng'  for using the English language
# '--oem 1' for using LSTM OCR Engine
config = r'--tessdata-dir "C:\Program Files (x86)\Tesseract-OCR\tessdata" -l eng --oem 1 --psm 3'

frame = cv2.imread("Serie2/IMG_1071.jpg")
frame = cv2.resize(frame, (0,0), fx=0.2, fy=0.2) 
hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)



"""
Range of colour yellow.
"""
lower = np.array([10,100,100])
upper = np.array([70,255,255])

"""
remove all colours but yellow and make a bit mask from it.
"""
mask = cv2.inRange(hsv, lower, upper)

"""
remove noise and features not needed
"""

kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)


"""
toon resultaat
"""
cv2.imshow('Original',frame)
cv2.imshow('edged' ,opening)

# Run tesseract OCR on image
text = pytesseract.image_to_string(opening, config=config)
# Print recognized text
print(text)

cv2.waitKey(0)
cv2.destroyAllWindows()

