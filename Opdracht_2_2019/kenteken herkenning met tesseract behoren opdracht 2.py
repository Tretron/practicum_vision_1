# -*- coding: utf-8 -*-
"""
Created on Fri Apr  5 09:06:13 2019

@author: laptop laurens
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 14:20:31 2019

@author: laptop laurens
"""

import cv2
import pytesseract
import numpy as np
  
 

pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe"
 

config = r'--tessdata-dir "C:\Program Files (x86)\Tesseract-OCR\tessdata" -l eng --oem 1 --psm 3'
 
"""
read image from disk and turn into HSV
"""
image = cv2.imread("Serie2/IMG_1071.jpg")
image = cv2.resize(image, (0,0), fx=0.2, fy=0.2) 

hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

"""
Range of colour yellow.
"""
lower = np.array([10,130,100])
upper = np.array([30,255,255])

"""
remove all colours but yellow and make a bit mask from it.
"""
mask = cv2.inRange(hsv, lower, upper)

"""
remove noise and features not needed
"""

kernel = np.ones((5,5),np.uint8)
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

gray = opening

thresh = cv2.threshold(gray, 0, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

coords = np.column_stack(np.where(thresh > 0))
angle = cv2.minAreaRect(coords)[-1]


if angle < -45:
	angle = -(90 + angle)


else:
	angle = -angle


(h, w) = image.shape[:2]
center = (w // 2, h // 2)
M = cv2.getRotationMatrix2D(center, angle, 1.0)
rotated = cv2.warpAffine(opening, M, (w, h),
	flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)



 # Run tesseract OCR on image
text = pytesseract.image_to_string(rotated, config=config)
# Print recognized text
print(text)
cv2.putText(image, text,
	(10, 50), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 3)
# show the output image

cv2.imshow("Input", image)
cv2.imshow("Rotated", rotated)
cv2.waitKey(0)
cv2.destroyAllWindows()